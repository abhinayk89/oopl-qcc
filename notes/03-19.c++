// -----------
// Tue, 19 Mar
// -----------

/*
N 850 C

Canvas
    ask and answer questions
    please be proactive
*/

int  i = 2;
int  v = i;
int& r = i;

int& f (int& r) { // returning an l-value
    ++r;
    return r;}

int f (int& r) { // returning an r-value
    ++r;
    return r;}

void f (int& r) {
    ++r;}

int  w = ?; // ? can  be an l-value or an r-value
int& x = ?; // ? must be an l-value

int  j = 2;
int* p = j;        // no
int* p = 2;        // no
int* p = 0;        // yes
int* p = nullptr;  // yes
int* p = &j;
++*p;
assert(j  == 3);
assert(*p == 3);
assert(p  == &j);
int k = 4;
p = &k;
++*p;

int i = 2;
int i = 3; // no

free(&i); // no

&i = ?; // no

void f (int* p) {
    if (!p)
        throw E(...);
    cout << p;  // the address
    cout << *p; // the value
    cout << &p; // the address of an address
    ++*p;}

void g (int& r) {
    ++*r;         // no
    ++r;}

int main () {
    int i = 2;
    f(&i);
    f(i);      // no
    f(2);      // no
    f(0);      // yes, but maybe undefined

    g(&i);     // no
    g(i);
    g(2);      // no
    g(0);      // no

    return 0;}

++(-i); // no
(-i)++; // no

a[2] = 3;
int& p = &a[2];

int* f () {
    int i = 2;
    return &i;}

int i = 2; // initialization is optional
++i;

int i;
i = 2;
++i;

const int ci = 2;
++ci;             // no

const int i;     // no

int        i = 2;
int        j = 3;
const int ci = 4;

int* p = &i; // read-write, many-location pointer
             // the pointer and the value are modifiable
++*p;
p = &j;
++*p;
p = &ci;     // no
++*p;

int        i = 2;
int        j = 3;
const int ci = 4;
const int cj = 5;

const int* pc = &ci; // read only, many location
const int* qc;
qc = &ci;
qc = &cj;
++*pc;               // no
++*qc;               // no

qc = &i;
++*qc;               // no
cout << *qc;         // would not be guaranteed to be the same over time

int* p = &i;

/*
1. pointer to constant int
2. pointer to int, but you can't later change the value of int *** this one ***
*/

int        i = 2;
int        j = 3;
const int ci = 4;
const int cj = 5;

int* const cp = &ci; // no, read-write, one location
int* const cq;       // no
int* const cr = &i;

int* const a = new int[100];
int*       b = a;
++b;
...
++*a;
++a[5];
++a;                         // no
...
delete [] a;

int        i = 2;
int        j = 3;
const int ci = 4;
const int cj = 5;

const int* const cpc;       // no, read only, one location pointer
const int* const cpc = &ci;
const int* const cqc = &i;
cpc = &cj;                  // no
++*cpc;                     // no

int* const a = new int[3];
++a;                       // no
...
delete [] a;

int a[] = {2, 3, 4}; // int* const
++a;                 // no

const int a[] = {2, 3, 4}; // const int* const
++a;                       // no

int a[] = {2, 3, 4};

/*
a's real nature is that of an array
++a won't compile
sizeof(a): size of the array
I can think of a as a pointer
*/

cout << *a;

int*  p = a;
int*  q = &a; // no
int** r = &a;

/*
p's real nature is that of a pointer
++p will compile
sizeof(p): size of a pointer
I can think of p as an array
*/

cout << p[1];

/*
In C casting is done with ()
*/

int    i = 2;
int*   p = &i;
float* q = p;         // no
float* q = (float*)p;

/*
In C++ there are four new named casts

1. const_cast : add or remove constness from a pointer or a reference
2. static_cast
3. reinterpret_cast
4. dynamic_cast

*/

int   i = 2;
float f = i;

// T is some user-defined type

struct T {
    T () {      // default constr
        ...}

    T (int i) {
        ...}};

int a[] = {2, 3, 4};
T   a[] = {2, 3, 4};

T x = 5; // T has a int constructor
T x(5);
T x{5};

const int s    = 10;
const int a[s] = {2, 3, 4};
const T   a[s] = {2, 3, 4};

T x;

U x(2, 3, 4); // 3-arg constr
U y(2, 3);    // 2-arg constr
U z(2);       // 1-arg constr
U s();        // declaration a function named s, that takes no args and returns a U
U t;          // 0-arg constr, default constr

int a[1000000] = {2, 3, 4}; // O(n)
int a[100000]; // O(1)

T a[1000000] = {2, 3, 4}; // O(n)
T a[100000]; // O(n), T() n times

int a[s] = {}; // O(n)
T   a[2] = {}; // O(n)

[5, 9)

int a[] = {...};
int b[] = {...};

cout << equal(a + 10, a + 15, b + 20);

/*
smallest a could be is: 15
smallest b could be is: 25
*/

// 7982

bool equal1 (const int* b, const int* e, const int* x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

bool equal2 (const int* b, const int* e, const long* x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

template <typename T1, typename T2>
bool equal (T1 b, T1 e, T2 x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

void test1 () {
    const int a[] = {2, 3, 4};
    const int b[] = {0, 2, 3, 4, 0};
    assert(!equal(a, a + 3, b));}
    // T1 -> const int*
    // T2 -> const int*

void test2 () {
    const int  a[] = {2, 3, 4};
    const long b[] = {0, 2, 3, 4, 0};
    assert(equal(a, a + 3, b + 1));}
    // T1 -> const int*
    // T2 -> const long*

vector<int> x(10, 2); // 10 ints, values 2
list<int>   y(10, 2); // 10 ints, values 2
deque<int>  z(10, 2); // 10 ints, values 2

int  a[] = {2, 3, 4};
int* b   = begin(a);
int* e   = end(a);

vector<int> x(10, 2);
vector<int>::iterator b = begin(x);
vector<int>::iterator e = end(x);

template <typename II1, typename II2>
bool equal (II1 b, II1 e, II2 x) {
    while (b != e) {
        if (*b != *x)
            return false;
        ++b;
        ++x;}
    return true;}

void test3 () {
    const list<int>  x = {2, 3, 4};
    const list<long> y = {0, 2, 3, 4, 0};
    assert(!equal(begin(x), end(x), begin(y)));}
    // T1 -> list<int>::const_iterator
    // T2 -> list<long>::const_iterator

// what operations does T1 need

!=
++
*

// what operations does T2 need

++
*

// input iterator
==, !=, ++, * (read only)

// output iterator
++, * (write only)

// forward iterator
<input iterator>, * (read/write)

// bidirectional iterator
<forward iterator>, --

// random-access iterator
<bidirectional iterator>, [], +, -, <, >, <=, >=

/*
algorithms should be defined to need    the weakest iterators
containers should be defined to provide the strongest iterators
*/

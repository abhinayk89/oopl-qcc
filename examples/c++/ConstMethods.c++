// ----------------
// ConstMethods.c++
// ----------------

#include <cassert>  // assert
#include <iostream> // cout, endl

template <typename T>
struct A {
    T        iv = 0;
    static T cv;

    void im ()
        {}

    static void cm ()
        {}

    void cim () const {
//      ++iv;           // error: increment of member 'A<int>::iv' in read-only object
        ++cv;

//      im();           // error: passing 'const A<int>' as 'this' argument discards qualifiers [-fpermissive]
        cm();}};

template <typename T>
T A<T>::cv = 0;

int main () {
    using namespace std;
    cout << "ConstMethods.c++" << endl;

//  A<int>::cm(); // error: cannot call member function 'void A<T>::cm() [with T = int]' without object

    A<int> x;
    assert(x.iv == 0);
    assert(x.cv == 0);

    x.cim();
    assert(x.iv == 0);
    assert(x.cv == 1);

    const A<int> y;
    y.cim();

    cout << "Done." << endl;
    return 0;}

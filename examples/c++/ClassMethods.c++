// ----------------
// ClassMethods.c++
// ----------------

#include <cassert>  // assert
#include <iostream> // cout, endl

template <typename T>
struct A {
    T        iv = 0;
    static T cv;

    void im ()
        {}

    void cim () const
        {}

    static void cm () {
//      ++iv;                     // error: invalid use of member 'A<T>::iv' in static member function
        ++cv;

//      assert(&cv == &this->cv); // error: invalid use of 'this' outside of a non-static member function
//      im();                     // error: call to non-static member function without an object argument
//      cim();                    // error: call to non-static member function without an object argument
        }};

template <typename T>
T A<T>::cv = 0;

int main () {
    using namespace std;
    cout << "ClassMethods.c++" << endl;

    assert(A<int>::cv == 0);

    A<int>::cm();
    assert(A<int>::cv == 1);

    A<int> x;
    x.cm();
    assert(A<int>::cv == 2);

    const A<int> y;
    y.cm();
    assert(A<int>::cv == 3);

    cout << "Done." << endl;
    return 0;}
